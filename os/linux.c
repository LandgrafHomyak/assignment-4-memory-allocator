#include <stddef.h>
#include <stdint.h>

#include <unistd.h>
#include <sys/mman.h>

#include <itmo_chat/_os.h>

size_t ItmoChat_OS_GetPageSize(void)
{
    return (size_t) (getpagesize());
}

void *ItmoChat_OS_AllocPages(const size_t pages_count)
{
    return mmap(NULL, pages_count * getpagesize(), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
}

int ItmoChat_OS_AppendPages(void const *const addr_after_last_page_in_chain, const size_t pages_count)
{
    void *actual_start = mmap((void *)addr_after_last_page_in_chain, pages_count * getpagesize(), PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    if (actual_start != addr_after_last_page_in_chain)
    {
        munmap((void *)actual_start, pages_count * getpagesize());
        return 1;
    }
    return 0;
}

void ItmoChat_OS_FreePages(void const *const start, const size_t pages_count)
{
    munmap((void *)start, pages_count * getpagesize());
}