#pragma once

#ifdef __cplusplus
# include <cstdder>
# include <cstdint>
#else
# include <stddef.h>
# include <stdint.h>
#endif

#ifdef __cplusplus
extern "C" {
#endif

size_t ItmoChat_OS_GetPageSize(void);

void *ItmoChat_OS_AllocPages(size_t pages_count);

int ItmoChat_OS_AppendPages(void const *addr_after_last_page_in_chain, size_t pages_count);

void ItmoChat_OS_FreePages(void const *start, size_t pages_count);

#ifdef __cplusplus
};
#endif
