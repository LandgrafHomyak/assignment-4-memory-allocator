#include <stddef.h>
#include <stdint.h>

#include <windows.h>

#include <itmo_chat/_os.h>

size_t ItmoChat_OS_GetPageSize(void)
{
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    return si.dwAllocationGranularity;
}

#define ItmoChat_OS__WinVirtualAlloc(START, SIZE) ((uintptr_t)(VirtualAlloc((void *)(START), (SIZE), MEM_COMMIT, PAGE_READWRITE)))
#define ItmoChat_OS__WinVirtualFree(START) (VirtualFree((void *)(START), 0, MEM_DECOMMIT))

void *ItmoChat_OS_AllocPages(const size_t pages_count)
{
    const size_t page_size = ItmoChat_OS_GetPageSize();
    /***************************************
     * DON'T DO LIKE THIS IN REAL PROGRAMS *
     ***************************************/
    uintptr_t first_page;
    uintptr_t current_page_expected;
    uintptr_t current_page_actual;
    size_t pages_left;

    first_page = ItmoChat_OS__WinVirtualAlloc(NULL, page_size);
    FIRST_PAGE_ALLOCATED:
    if ((void *) first_page == NULL)
        return NULL;

    current_page_expected = first_page + page_size;
    for (pages_left = pages_count - 1; pages_left > 0; pages_left--)
    {
        if ((current_page_actual = ItmoChat_OS__WinVirtualAlloc(NULL, page_size)) != current_page_expected)
        {
            first_page = current_page_actual;
            goto FREE_CHAIN;
        }
        current_page_expected += page_size;
        if (current_page_expected < current_page_actual) /* if overflow */
        {
            first_page = (uintptr_t) NULL;
            goto FREE_CHAIN;
        }
    }
    return (void *) first_page;

    FREE_CHAIN:
    for (pages_left = pages_count - pages_left; pages_left > 0; pages_left--) {
        ItmoChat_OS__WinVirtualFree((current_page_expected -= page_size));
    }

    goto FIRST_PAGE_ALLOCATED;
}

int ItmoChat_OS_AppendPages(void const *const addr_after_last_page_in_chain, const size_t pages_count)
{
    const size_t page_size = ItmoChat_OS_GetPageSize();
    /***************************************
     * DON'T DO LIKE THIS IN REAL PROGRAMS *
     ***************************************/
    uintptr_t first_page;
    uintptr_t current_page_expected;
    uintptr_t current_page_actual;
    size_t pages_left;

    first_page = ItmoChat_OS__WinVirtualAlloc(addr_after_last_page_in_chain, page_size);
    if ((void *) first_page == NULL)
        return 1;
    if ((void *) first_page != addr_after_last_page_in_chain) {
        ItmoChat_OS__WinVirtualFree(first_page);
        return 1;
    }

    current_page_expected = first_page + page_size;
    for (pages_left = pages_count - 1; pages_left > 0; pages_left--)
    {
        if ((current_page_actual = ItmoChat_OS__WinVirtualAlloc(NULL, page_size)) != current_page_expected)
            goto FREE_CHAIN;

        current_page_expected += page_size;
        if (current_page_expected < current_page_actual) /* if overflow */
            goto FREE_CHAIN;
    }
    return 0;

    FREE_CHAIN:
    for (pages_left = pages_count - pages_left; pages_left > 0; pages_left--) {
        ItmoChat_OS__WinVirtualFree((current_page_expected -= page_size));
    }

    return 1;
}

void ItmoChat_OS_FreePages(void const *const start, size_t pages_count) {
    const size_t page_size = ItmoChat_OS_GetPageSize();
    uintptr_t current_page = (uintptr_t) start;
    while (pages_count-- > 0) {
        ItmoChat_OS__WinVirtualFree(current_page);
        current_page += page_size;
    }
}