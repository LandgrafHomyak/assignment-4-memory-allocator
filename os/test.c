#include <stddef.h>
#include <stdint.h>

#include <itmo_chat/_os.h>

#define mp(PTR, PC) ((void *)(((uintptr_t)(PTR)) + ItmoChat_OS_GetPageSize() * (PC)))

int main(int argc, char **argv)
{
    void *p = ItmoChat_OS_AllocPages(1);
    if (p == NULL)
        return 1;

    ItmoChat_OS_FreePages(p, 1);
    p = ItmoChat_OS_AllocPages(3);
    if (p == NULL)
        return 1;
    ItmoChat_OS_FreePages(mp(p, 1), 1);
    if (ItmoChat_OS_AppendPages(mp(p, 1), 1))
    {
        return 1;
    }
    ItmoChat_OS_FreePages(mp(p, 1), 1);
    if (!ItmoChat_OS_AppendPages(mp(p, 1), 2))
    {
        return 1;
    }
    ItmoChat_OS_FreePages(p, 1);
    ItmoChat_OS_FreePages(mp(p, 2), 1);

    return 0;
}