// Константа обозначающая, что сообщение не является комментарием к посту.
#define ItmoChat_Chat_NOT_A_COMMENT ((int)(0))

// Инкапсулирует в себя основную метаинформацию об отдельном чате.
struct ItmoChat_Chat
{
    /* TODO */
};

// Информация о сообщении, сам текст сообщение идёт сразу за этой структурой.
struct ItmoChat_Message
{
    const unsigned int post;
    const unsigned long time;
    const char text[];
};

// Конструктор чата, инициализирует поля в структуре.
void ItmoChat_Chat_Init(struct ItmoChat_Chat *self);

// Деструктор чата, освобождает все использованные ресурсы.
void ItmoChat_Chat_Deinit(struct ItmoChat_Chat *self);

// Добавляет сообщение в историю
// Возвращает 0 в случае успеха, и ненулевое значение если произошла ошибка (например, закончилась память).
int ItmoChat_Chat_AddMessage(struct ItmoChat_Chat *self, unsigned int post, const unsigned long time, char const *text);

#define _ItmoChat_Chat_AddMessageF_NOT_IMPLEMENTED ((unsigned int)-2)
#define ItmoChat_Chat_AddMessageF_FAILED ((unsigned int)-1)

// Добавляет сообщение в историю используя форматирование аналогично printf.
// Возвращает длину получившегося сообщения, или ItmoChat_Chat_AddMessageF_FAILED в случае ошибки.
// Дополнительное задание, опционально для выполнения.
unsigned int ItmoChat_Chat_AddMessageF(struct ItmoChat_Chat *self, unsigned int post, const unsigned long time, char const *format, ...);

// Содержит информацию о текущей позиции при переборе всех сообщений.
struct ItmoChat_Chat_RIterator
{
    /* TODO */
};

// Возвращает итератор для перебора всех сообщений.
struct ItmoChat_Chat_RIterator ItmoChat_Chat_RIter(struct ItmoChat_Chat const *self);

// Возвращает следующее сообщение (т.к. мы идём с конца, то предыдущее в истории) или NULL если достигнут конец.
struct ItmoChat_Message const *ItmoChat_Chat_RIterator_Next(struct ItmoChat_Chat_RIter *it);

// Содержит информацию о текущей позиции при переборе сообщений определённого типа.
struct ItmoChat_Chat_RTypeIterator
{
    /* TODO */
};

// Возвращает итератор для перебора всех сообщений.
struct ItmoChat_Chat_RTypeIterator ItmoChat_Chat_RTypeIter(struct ItmoChat_Chat const *self, int type);

// Возвращает следующее сообщение соответствующего типа (т.к. мы идём с конца, то предыдущее в истории) или NULL если достигнут конец.
struct ItmoChat_Message const *ItmoChat_Chat_RTypeIterator_Next(struct ItmoChat_Chat_RTypeIter *it);

// Удаляет старые сообщения до тех пор, пока размер чата не будет меньше либо равен capacity.
int ItmoChat_Chat_Crop(struct ItmoChat_Chat const *self, size_t capacity);

// Удаляет сообщение по указателю (фича для модераторов чата).
int ItmoChat_Chat_DeleteMessage(struct ItmoChat_Chat const *self, struct ItmoChat_Message const *message);